import "./MainContent.scss";
import Card from "../Shared/Card/Card.jsx";

const MainContent = ({ jobs }) => {
  return (
    <>
      <div className="card__list">
        {jobs.map((job) => (
          <Card key={job.id} job={job} />
        ))}
      </div>
    </>
  );
};

export default MainContent;
