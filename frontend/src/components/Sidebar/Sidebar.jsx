import "./Sidebar.scss";

const Sidebar = () => {
  return (
    <div className="sidebar">
      <header className="sidebar__header">
        <h2>
          <span>Join</span> Our Team
        </h2>
      </header>
      <main className="sidebar__body">
        <div className="sidebar__group">
          <h3 className="sidebar__group-title">Employment Type</h3>
          <ul className="sidebar__group-list">
            <li className="sidebar__item">
              <input type="checkbox" name="employment" id="1" />
              <label htmlFor="1">Contract</label>
            </li>
            <li className="sidebar__item">
              <input type="checkbox" name="employment" id="2" />
              <label htmlFor="2">Internship</label>
            </li>
            <li className="sidebar__item">
              <input type="checkbox" name="employment" id="3" />
              <label htmlFor="3">Full-Time</label>
            </li>
            <li className="sidebar__item">
              <input type="checkbox" name="employment" id="4" />
              <label htmlFor="4">Temporary</label>
            </li>
            <li className="sidebar__item">
              <input type="checkbox" name="employment" id="5" />
              <label htmlFor="5">Part-Time</label>
            </li>
          </ul>
        </div>
        <div className="sidebar__group">
          <h3 className="sidebar__group-title">Position Level</h3>
          <ul className="sidebar__group-list">
            <li className="sidebar__item">
              <input type="checkbox" name="position" id="pos-1" />
              <label htmlFor="pos-1">CEO/GM/Direktur/Manajer Senior</label>
            </li>
            <li className="sidebar__item">
              <input type="checkbox" name="position" id="pos-2" />
              <label htmlFor="pos-2">Manajer/Asisten Manajer</label>
            </li>
            <li className="sidebar__item">
              <input type="checkbox" name="position" id="pos-3" />
              <label htmlFor="pos-3">Supervisor/Koordinator</label>
            </li>
            <li className="sidebar__item">
              <input type="checkbox" name="position" id="pos-4" />
              <label htmlFor="pos-4">
                Pegawai non-manajemen & non-supervisor
              </label>
            </li>
            <li className="sidebar__item">
              <input type="checkbox" name="position" id="pos-5" />
              <label htmlFor="pos-5">
                Lulusan baru/Pengalaman kerja kurang dari 1 tahun
              </label>
            </li>
          </ul>
        </div>
      </main>
    </div>
  );
};

export default Sidebar;
