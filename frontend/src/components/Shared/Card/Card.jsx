import "./Card.scss";
import Button from "../Button/Button";
import { useNavigate } from "react-router-dom";

const Card = ({ job }) => {
  const navigate = useNavigate();

  return (
    <div className="card">
      <div className="card__header">
        <h3>{job.title}</h3>
      </div>
      <div
        className="card__body"
        dangerouslySetInnerHTML={{ __html: job.description }}
      ></div>
      <div className="card__footer">
        <Button onClick={() => navigate(`jobs/${job.id}`)} variant="text">
          View Details
        </Button>
        <Button variant="primary">Apply</Button>
      </div>
    </div>
  );
};

export default Card;
