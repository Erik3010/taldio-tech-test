import Sidebar from "../../components/Sidebar/Sidebar.jsx";
import MainContent from "../../components/MainContent/MainContent";

import "./Home.scss";
import { useState, useEffect } from "react";
import http from "../../http";

const Home = () => {
  const [jobs, setJobs] = useState([]);

  useEffect(() => {
    const getJobs = async () => {
      const response = await http.get("jobs");
      setJobs(response.data);
    };

    getJobs();
  }, []);

  return (
    <>
      <div className="container home-container">
        <Sidebar />
        <MainContent jobs={jobs} />
      </div>
    </>
  );
};

export default Home;
