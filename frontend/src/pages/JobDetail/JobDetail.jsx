// import { useParams } from "react-router-dom";
import "./JobDetail.scss";
import Button from "../../components/Shared/Button/Button";
import { useParams } from "react-router-dom";
import { useState, useEffect } from "react";
import http from "../../http";

const JobDetail = () => {
  const [job, setJob] = useState([]);
  const { id } = useParams();

  useEffect(() => {
    const getJobDetail = async () => {
      const response = await http.get(`jobs/${id}`);
      setJob(response.data);
    };

    getJobDetail();
  }, [id]);

  return (
    <main className="section">
      <header className="section__header">
        <div className="container">
          <div className="section__title">
            <h2>{job.title}</h2>
            <h3>
              {job.position_level} | {job.employment_type}
            </h3>
          </div>
          <Button variant="primary">Apply</Button>
        </div>
      </header>
      <div className="section__body">
        <div className="container">
          <div className="section__item">
            <h4>Job Description</h4>
            <div dangerouslySetInnerHTML={{ __html: job.description }}></div>
          </div>
          <div className="section__item">
            <h4>Minimum Qualification</h4>
            <div
              dangerouslySetInnerHTML={{ __html: job.min_qualification }}
            ></div>
          </div>
          <div className="section__item">
            <h4>Minimum Experience</h4>
            <p>{job.years_of_experience} year(s)</p>
          </div>
          <div className="section__item">
            <h4>Skills</h4>
            <ul>
              {job.job_skills?.map((skill) => (
                <li key={skill.id}>{skill.skill.name}</li>
              ))}
            </ul>
          </div>
          <div className="section__item">
            <h4>Benefit</h4>
            <p>{job.benefits}</p>
          </div>
        </div>
      </div>
    </main>
  );
};

export default JobDetail;
