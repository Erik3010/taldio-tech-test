package models

import (
	"log"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var db *gorm.DB

func Setup() {
	var err error

	dsn := "root:@tcp(127.0.0.1)/taldio_test?charset=utf8mb4&parseTime=True&loc=Local"
	db, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})

	if err != nil {
		log.Fatal(err.Error())
	}

	// CreateJob()
	// CreateSkills()
	// CreateJobSkills()

	// db.AutoMigrate(&Job{})
}