package models

type Skill struct {
	ID uint `json:"id"`
	Name string `json:"name"`
}

func CreateSkills() error {
	skills := []Skill{
		Skill{Name: "HTML"},
		Skill{Name: "CSS"},
		Skill{Name: "Javascript"},
		Skill{Name: "PHP"},
		Skill{Name: "MySQL"},
		Skill{Name: "React JS"},
		Skill{Name: "Node JS"},
		Skill{Name: "Vue JS"},
		Skill{Name: "Java"},
		Skill{Name: "React Native"},
		Skill{Name: "Typescript"},
		Skill{Name: "Flutter"},
		Skill{Name: "Go"},
	}

	err := db.Create(&skills).Error

	return err
}