package models

import (
	// "gorm.io/gorm"
)

type Job struct {
	ID uint `json:"id"`
	Title string `json:"title"`
	Description string `json:"description"`
	PositionLevel string `json:"position_level"`
	MinQualification string `json:"min_qualification"`
	EmploymentType string `json:"employment_type"`
	YearsOfExperience int `json:"years_of_experience"`
	Benefits string `json:"benefits"`
	JobSkills []JobSkill `json:"job_skills" gorm:"foreignKey:JobId"`
}

func GetJobs() []Job {
	var jobs []Job

	db.Preload("JobSkills").Preload("JobSkills.Skill").Find(&jobs)

	return jobs
}

func GetJobById(id int) *Job {
	var job Job
	result := db.Preload("JobSkills").Preload("JobSkills.Skill").First(&job, id)

	if result.Error != nil {
		return nil
	}
	
	return &job
}

func CreateJob() error {
	jobs := []Job{
		Job{
			Title: "Database Administrator",
			Description: "<p>- Menguasai &nbsp;ms sql, sql cluster, sql replication/ha, tuning query, function/store procedure<br>- Pengalaman : Minimum 2 tahun sebagai DBA<br>&nbsp;</p>",
			PositionLevel: "Pegawai non-manajemen & non-supervisor",
			MinQualification: "<p>Pendidikan terakhir D3/S1 jurusan terkait</p>",
			EmploymentType: "contract",
			YearsOfExperience: 2,
			Benefits: "-",
		},
		Job{
			Title: "PHP Developer",
			Description: "<p>-</p>",
			PositionLevel: "Pegawai non-manajemen & non-supervisor",
			MinQualification: "<p><strong>Minimum Qualification</strong></p><ul><li>Minimal pengalaman sebagai progammer 2 tahun / open for Fresh Graduate</li><li>Pengalaman project menggunakan PHP</li><li>Disukai yg bisa bekerja dalam team</li><li>Mampu menggunakan database ssql, nosql, mysql</li><li>Mempunyai kperibadian yang baik didalam team</li><li>Dapat bekerja sendiri maupun team</li><li>Mempunyai inisiatif sendiri tanpa harus diawasi untuk mencapai objekti</li></ul>",
			EmploymentType: "contract",
			YearsOfExperience: 1,
			Benefits: "-",
		},
		Job{
			Title: "Golang Developer",
			Description: "<p>Experinence Angular 6+, HTML5, CSS3 and builfing cross browser Apps</p><p>Extentive Knowledge of web application</p><p>Experience RESTful API you applicants</p><p>Develop using API using Angular</p><p>Integrate API with frontend user web component</p><p>Willing to be work on site in Jakarta</p>",
			PositionLevel: "Pegawai non-manajemen & non-supervisor",
			MinQualification: "<p>Experinence Angular 6+, HTML5, CSS3 and builfing cross browser Apps</p><p>Extentive Knowledge of web application</p><p>Experience RESTful API you applicants</p><p>Develop using API using Angular</p><p>Integrate API with frontend user web component</p><p>Willing to be work on site in Jakarta</p>",
			EmploymentType: "contract",
			YearsOfExperience: 1,
			Benefits: "Health Insurance",
		},
	}

	err := db.Create(&jobs).Error

	return err
}