package models

type JobSkill struct {
	ID uint `json:"id"`
	JobId int `json:"job_id"`
	SkillId int `json:"skill_id"`
	Skill Skill `json:"skill"`
}

func CreateJobSkills() error {
	jobSkills := []JobSkill{
		JobSkill{JobId: 1, SkillId: 1},
		JobSkill{JobId: 1, SkillId: 2},
		JobSkill{JobId: 1, SkillId: 3},
		JobSkill{JobId: 2, SkillId: 4},
		JobSkill{JobId: 2, SkillId: 5},
		JobSkill{JobId: 3, SkillId: 8},
	}

	err := db.Create(&jobSkills).Error

	return err
}