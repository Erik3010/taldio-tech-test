-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 04 Agu 2023 pada 12.49
-- Versi server: 10.4.19-MariaDB
-- Versi PHP: 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `taldio_test`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `jobs`
--

CREATE TABLE `jobs` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text DEFAULT NULL,
  `position_level` varchar(255) NOT NULL,
  `min_qualification` text DEFAULT NULL,
  `employment_type` enum('contract','internship','full-time','temporary','part-time') NOT NULL,
  `years_of_experience` int(11) NOT NULL,
  `benefits` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `jobs`
--

INSERT INTO `jobs` (`id`, `title`, `description`, `position_level`, `min_qualification`, `employment_type`, `years_of_experience`, `benefits`, `created_at`, `updated_at`) VALUES
(1, 'Database Administrator', '<p>- Menguasai &nbsp;ms sql, sql cluster, sql replication/ha, tuning query, function/store procedure<br>- Pengalaman : Minimum 2 tahun sebagai DBA<br>&nbsp;</p>', 'Pegawai non-manajemen & non-supervisor', '<p>Pendidikan terakhir D3/S1 jurusan terkait</p>', 'contract', 2, '-', '2023-08-04 04:50:04', '2023-08-04 04:50:04'),
(2, 'PHP Developer', '<p>-</p>', 'Pegawai non-manajemen & non-supervisor', '<p><strong>Minimum Qualification</strong></p><ul><li>Minimal pengalaman sebagai progammer 2 tahun / open for Fresh Graduate</li><li>Pengalaman project menggunakan PHP</li><li>Disukai yg bisa bekerja dalam team</li><li>Mampu menggunakan database ssql, nosql, mysql</li><li>Mempunyai kperibadian yang baik didalam team</li><li>Dapat bekerja sendiri maupun team</li><li>Mempunyai inisiatif sendiri tanpa harus diawasi untuk mencapai objekti</li></ul>', 'contract', 1, '-', '2023-08-04 04:50:04', '2023-08-04 04:50:04'),
(3, 'Golang Developer', '<p>Experinence Angular 6+, HTML5, CSS3 and builfing cross browser Apps</p><p>Extentive Knowledge of web application</p><p>Experience RESTful API you applicants</p><p>Develop using API using Angular</p><p>Integrate API with frontend user web component</p><p>Willing to be work on site in Jakarta</p>', 'Pegawai non-manajemen & non-supervisor', '<p>Experinence Angular 6+, HTML5, CSS3 and builfing cross browser Apps</p><p>Extentive Knowledge of web application</p><p>Experience RESTful API you applicants</p><p>Develop using API using Angular</p><p>Integrate API with frontend user web component</p><p>Willing to be work on site in Jakarta</p>', 'contract', 1, 'Health Insurance', '2023-08-04 04:50:04', '2023-08-04 04:50:04');

-- --------------------------------------------------------

--
-- Struktur dari tabel `job_skills`
--

CREATE TABLE `job_skills` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `skill_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `job_skills`
--

INSERT INTO `job_skills` (`id`, `job_id`, `skill_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2023-08-04 04:50:04', '2023-08-04 04:50:04'),
(2, 1, 2, '2023-08-04 04:50:04', '2023-08-04 04:50:04'),
(3, 1, 3, '2023-08-04 04:50:04', '2023-08-04 04:50:04'),
(4, 2, 4, '2023-08-04 04:50:04', '2023-08-04 04:50:04'),
(5, 2, 5, '2023-08-04 04:50:04', '2023-08-04 04:50:04'),
(6, 3, 8, '2023-08-04 04:50:04', '2023-08-04 04:50:04');

-- --------------------------------------------------------

--
-- Struktur dari tabel `skills`
--

CREATE TABLE `skills` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `skills`
--

INSERT INTO `skills` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'HTML', '2023-08-04 04:50:04', '2023-08-04 04:50:04'),
(2, 'CSS', '2023-08-04 04:50:04', '2023-08-04 04:50:04'),
(3, 'Javascript', '2023-08-04 04:50:04', '2023-08-04 04:50:04'),
(4, 'PHP', '2023-08-04 04:50:04', '2023-08-04 04:50:04'),
(5, 'MySQL', '2023-08-04 04:50:04', '2023-08-04 04:50:04'),
(6, 'React JS', '2023-08-04 04:50:04', '2023-08-04 04:50:04'),
(7, 'Node JS', '2023-08-04 04:50:04', '2023-08-04 04:50:04'),
(8, 'Vue JS', '2023-08-04 04:50:04', '2023-08-04 04:50:04'),
(9, 'Java', '2023-08-04 04:50:04', '2023-08-04 04:50:04'),
(10, 'React Native', '2023-08-04 04:50:04', '2023-08-04 04:50:04'),
(11, 'Typescript', '2023-08-04 04:50:04', '2023-08-04 04:50:04'),
(12, 'Flutter', '2023-08-04 04:50:04', '2023-08-04 04:50:04'),
(13, 'Go', '2023-08-04 04:50:04', '2023-08-04 04:50:04');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `job_skills`
--
ALTER TABLE `job_skills`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `skills`
--
ALTER TABLE `skills`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `job_skills`
--
ALTER TABLE `job_skills`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `skills`
--
ALTER TABLE `skills`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
