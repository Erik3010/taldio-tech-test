package router

import (
	"taldio-backend/controllers/jobs_controller"

	"github.com/gin-gonic/gin"
)

func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Credentials", "true")
        c.Header("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
        c.Header("Access-Control-Allow-Methods", "POST,HEAD,PATCH, OPTIONS, GET, PUT")

        if c.Request.Method == "OPTIONS" {
            c.AbortWithStatus(204)
            return
        }

		c.Next()
	}
}

func InitRouter() *gin.Engine {
	r := gin.Default()
	r.Use(CORSMiddleware())

	apiV1 := r.Group("/api/v1")

	apiV1.GET("/jobs", jobs_controller.GetAll)
	apiV1.GET("/jobs/:id", jobs_controller.GetById)

	return r
}