package jobs_controller

import (
	"strconv"

	"taldio-backend/models"

	"net/http"

	// "gorm.io/gorm"
	"github.com/gin-gonic/gin"
)

func GetAll(c *gin.Context) {
	jobs := models.GetJobs()

	c.JSON(http.StatusOK, jobs)
}

func GetById(c *gin.Context) {
	idStr := c.Param("id")

	id, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid ID"})
		return
	}

	job := models.GetJobById(id)

	if job == nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Job not found"})
		return
	}

	c.JSON(http.StatusOK, job)
}