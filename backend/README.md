# Taldio Backend API

## How to Run

Create a database with the name `taldio_test`.
Import the `taldio_test.sql` file to the database

To run the Backend API, run the following command in the terminal and the server will run on port 8080

```bash
go run main.go
```
