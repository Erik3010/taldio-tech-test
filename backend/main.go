package main


import (
    "taldio-backend/models"
    "taldio-backend/router"
)

func main() {
    models.Setup()

	router := router.InitRouter()

	router.Run()
}